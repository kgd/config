#!/bin/bash

export JAVA_HOME=/home/${USER}/Tools/java
export GRAILS_HOME=/home/${USER}/Tools/grails

export PATH=$JAVA_HOME/bin:$GRAILS_HOME/bin/:$PATH

alias rapid=' gnome-terminal --window-with-profile=dev -e "sshpass -p dipar ssh rapid@p4alarmy.touk.pl"'
alias selenium=' gnome-terminal --window-with-profile=dev -e "ssh vnc@p4alarmy-selenium-debian.touk.pl"'
alias preprod2=' gnome-terminal --window-with-profile=prod -e "sshpass -p ross ssh ross@172.17.5.16"'
alias preprod1=' gnome-terminal --window-with-profile=prod -e "sshpass -p ross ssh ross@172.16.35.27"'

alias correlator='sshpass -p ross ssh ross@172.17.5.16 ls correlator; echo ""; echo ""; sshpass -p ross ssh ross@172.16.35.27 ls correlator'

alias ll='ls -la'

alias install="sudo apt-get install"
alias duu="du -h -d 1 -a 2>/dev/null | sort -h "


alias tarls="tar -tf"
alias tarp="/home/kgd/scripts/tarpack.sh"
alias taru="tar xvzf"

alias cleandocker="/home/kgd/scripts/clean_docker.sh"

alias new='gnome-terminal --working-directory=`pwd`'
alias gitb="/home/$USER/scripts/git_back.sh"
alias gitf="/home/$USER/scripts/git_fix.sh"
alias gitff="/home/$USER/scripts/git_fix.sh push"
alias gitc="/home/$USER/scripts/git_clean.sh $1"
alias gitpf="git push -f"

alias backend="cd ~/work/alarmy/RapidServer/RapidSuite/"
alias frontend="cd ~/work/alarmy/client"
alias logsrapid="less ~/work/alarmy/RapidServer/RapidSuite/target/work/tomcat/logs/RapidServer.log"

alias gui="nautilus . &"

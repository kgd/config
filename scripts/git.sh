#!/bin/bash

#identity
git config --global user.name "Krzysztof Gadomski"
git config --global user.email kgd@touk.pl

#aliases
git config --global alias.please 'push --force-with-lease'
git config --global alias.commend 'commit --amend --no-edit'
git config --global alias.it '!git init && git commit -m “root” --allow-empty'
git config --global alias.st 'status --short --branch'
git config --global alias.grog 'log --graph --abbrev-commit --decorate --all --format=format:"%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(dim white) - %an%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n %C(white)%s%C(reset)"'
git config --global alias.reb 'rebase -i origin/development'
git config --global alias.ori "reset --hard $(git rev-parse --abbrev-ref --symbolic-full-name @{u})"

#other
git config --global core.editor vim

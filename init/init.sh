#!/bin/bash

sudo apt-get update
sudo apt-get install -y vim htop git maven sshpass docker.io chrome-gnome-shell lm-sensors curl gnome-tweak-tool blueman qnapi net-tools 

sudo usermod -aG docker $USER

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash

# libinput gestures
sudo apt-get -y install xdotool wmctrl libinput-tools
libinput-gestures-setup start
libinput-gestures-setup autostart
sudo gpasswd -a $USER input

# torrents
sudo apt-get remove transmission-gtk
sudo apt-get install deluge 

#nvm install node
#npm i -g yarn

# sudo apt install 
#openjdk-8-jdk
# tlp

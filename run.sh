#!/bin/bash

# packages
sudo add-apt-repository ppa:fixnix/netspeed
sudo apt-get update && sudo apt-get install -y git clipit unity-tweak-tool vim nmap indicator-netspeed-unity 

# copy config files
# cp home/* ~/

# run all scripts
for file in scripts/*; do $file 2>/dev/null; done

#! /bin/bash

docker rm -f $(docker ps -a -q)

if [ $1 == "i" ]; then 
  docker rmi -f $(docker images -q)
fi

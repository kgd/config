docker rm -f rapid_db
docker run -td --restart always --name rapid_db -p 5431:5432 -p 8983:8983 -p 61616:61616 touk/rapid_integration_tests:1.4 bash -c "/etc/init.d/postgresql start; /rapidoss/solr/bin/solr start; /rapidoss/activemq/bin/activemq start & sleep 30d; "

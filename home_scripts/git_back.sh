#!/bin/bash

git checkout $(git reflog | grep checkout | head -1 | cut -d: -f3 | awk '{print $3}')

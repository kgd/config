#!/bin/bash

git add .
git commit --amend --no-edit

if [[ "$1" == "push" ]];  
then
	git push -f
fi

#!/bin/bash

if [ "$1" == "same" ]; then
  cd /rapidoss/jetty/ &&  java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8787 -jar start.jar
else
 mv /rapidoss/jetty/webapps/*.war /tmp/ 
 cd ~/work/alarmy/RapidServer/RapidSuite/ && mvn clean install -DskipTests=true && mv target/*.war /rapidoss/jetty/webapps/ && cd /rapidoss/jetty/ &&  java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8787 -jar start.jar
fi

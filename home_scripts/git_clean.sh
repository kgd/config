#!/bin/bash

git checkout development
git pull

if [[ ! -z $1 ]];  
then
	git checkout -b $1
fi

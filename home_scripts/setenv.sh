#!/bin/bash

export JAVA_HOME=/home/kgd/Tools/java
export GRAILS_HOME=/home/kgd/Tools/grails

scripts=/home/kgd/scripts/

export PATH=$scripts:$JAVA_HOME/bin:$GRAILS_HOME/bin/:$PATH

export RS_CONFIG=/rapidoss/jetty/etc/rapidoss.properties
